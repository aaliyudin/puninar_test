<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function test()
    {
      for($i=0;$i<=5;$i++){
        for($j=5-$i;$j>=1;$j--){
          echo $j;
        }
        echo "<br>";
      }
      echo "<br>";

      $n=5;
      for($i=0; $i<=$n; $i++)
      {
        for($j=1; $j<=$i; $j++)
        echo "&nbsp;";
        for($k=1; $k<=$n-$i; $k++)
        echo $k;
        for($j=($k-2); $j>0; $j--)
        echo $j;
        for($k=1; $k<=$i; $k++)
        echo "&nbsp;";
        echo "</br>";
      }
    }
}
