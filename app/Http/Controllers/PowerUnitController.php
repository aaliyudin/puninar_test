<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUpdatePowerUnitFormRequest;
use Illuminate\Support\Facades\Storage;
use App\Models\PowerUnit;
use App\Models\PowerUnitType;
use App\Models\Location;
use App\Models\Corporation;
use Auth;
use Illuminate\Http\Request;

class PowerUnitController extends Controller
{
  private $powerUnit;

  public function __construct(PowerUnit $powerUnit)
  {
    $this->power_unit = $powerUnit;

    $this->middleware('auth')
    ->except([
      'index', 'show'
    ]);
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    $power_units = $this->power_unit->latest()->paginate();

    return view('power_units.index', compact('power_units'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $data['power_unit_types'] = PowerUnitType::all();
    $data['locations'] = Location::all();
    $data['corporations'] = Corporation::all();

    return view('power_units.create', compact('data'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \App\Http\Requests\StoreUpdatePowerUnitFormRequest  $request
  * @return \Illuminate\Http\Response
  */
  public function store(StoreUpdatePowerUnitFormRequest $request)
  {
    $data = $request->all();

    $powerUnit = new PowerUnit;
    $powerUnit->power_unit_num = $data['power_unit_num'];
    $powerUnit->description = $data['description'];
    $powerUnit->id_corporation = $data['id_corporation'];
    $powerUnit->id_location = $data['id_location'];
    $powerUnit->id_power_unit_type = $data['id_power_unit_type'];
    $powerUnit->insert_user = Auth::user()->id;
    $powerUnit->save();

    return redirect()
    ->route('power_unit.index')
    ->withSuccess('Create data success');
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    if (!$power_unit = $this->power_unit->find($id))
    return redirect()->back();

    return view('power_units.show', compact('power_unit'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    if (!$power_unit = $this->power_unit->find($id))
    return redirect()->back();
    $data['power_unit_types'] = PowerUnitType::all();
    $data['locations'] = Location::all();
    $data['corporations'] = Corporation::all();
    $data['power_unit'] = $power_unit;

    return view('power_units.edit', compact('data'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \App\Http\Requests\StoreUpdatePowerUnitFormRequest  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(StoreUpdatePowerUnitFormRequest $request, $id)
  {
    if (!$powerUnit = $this->power_unit->find($id))
    return redirect()->back();

    $data = $request->all();
    $data['update_user'] = Auth::user()->id;

    $powerUnit->update($data);

    return redirect()
    ->route('power_unit.index')
    ->withSuccess('Update data success!');
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    if (!$powerUnit = $this->power_unit->find($id))
    return redirect()->back();

    $powerUnit->delete();

    return redirect()
    ->route('power_unit.index')
    ->withSuccess('Delete data success!');
  }
}
