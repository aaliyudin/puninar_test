<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\StoreUpdatePowerUnitFormRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\PowerUnit;
use App\Models\PowerUnitType;
use App\Models\Location;
use App\Models\Corporation;
use Auth;
use Illuminate\Http\Request;
use JWTFactory;
use JWTAuth;

class PowerUnitController extends Controller
{
  private $powerUnit;

  public function __construct(PowerUnit $powerUnit)
  {
    $this->power_unit = $powerUnit;
  }

  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    try {
      $power_units = $this->power_unit->latest()->paginate();

      $this->return['status'] = 'success';
      $this->return['status_code'] = 200;
      $this->return['data'] = $power_units;
    } catch (\Exception $e) {
      $this->return['status'] = 'error';
      $this->return['status_code'] = 400;
      $this->return['message'] = [$e->getMessage()];
    }

    return response()->json($this->return);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */

  /**
  * Store a newly created resource in storage.
  *
  * @param  \App\Http\Requests\StoreUpdatePowerUnitFormRequest  $request
  * @return \Illuminate\Http\Response
  */
  public function store(StoreUpdatePowerUnitFormRequest $request)
  {
    $data = $request->all();

    try {
      $powerUnit = new PowerUnit;
      $powerUnit->power_unit_num = $data['power_unit_num'];
      $powerUnit->description = $data['description'];
      $powerUnit->id_corporation = $data['id_corporation'];
      $powerUnit->id_location = $data['id_location'];
      $powerUnit->id_power_unit_type = $data['id_power_unit_type'];
      $powerUnit->insert_user = auth()->user()->id;
      $powerUnit->save();

      $this->return['status'] = 'success';
      $this->return['status_code'] = 200;
      $this->return['data'] = $powerUnit;
    } catch (\Exception $e) {
      $this->return['status'] = 'error';
      $this->return['status_code'] = 400;
      $this->return['message'] = [$e->getMessage()];
    }

    return response()->json($this->return);
  }
  /**
  * Update the specified resource in storage.
  *
  * @param  \App\Http\Requests\StoreUpdatePowerUnitFormRequest  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(StoreUpdatePowerUnitFormRequest $request, $id)
  {
    if (!$powerUnit = $this->power_unit->find($id)){
      $this->return['status'] = 'error';
      $this->return['status_code'] = 400;
      $this->return['message'] = 'No record for these data';
    } else {
      try {
        $data = $request->all();
        $data['update_user'] = auth()->user()->id;

        $powerUnit->update($data);

        $this->return['status'] = 'success';
        $this->return['status_code'] = 200;
        $this->return['data'] = $data;
        $this->return['message'] = 'Update data success';
      } catch (\Exception $e) {
        $this->return['status'] = 'error';
        $this->return['status_code'] = 400;
        $this->return['message'] = [$e->getMessage()];
      }
    }
    return response()->json($this->return);
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    if (!$powerUnit = $this->power_unit->find($id)){
      $this->return['status'] = 'error';
      $this->return['status_code'] = 400;
      $this->return['message'] = 'No record for these data';
    } else {
      try {
        $powerUnit->delete();

        $this->return['status'] = 'success';
        $this->return['status_code'] = 200;
        $this->return['message'] = 'Delete data success';
      } catch (\Exception $e) {
        $this->return['status'] = 'error';
        $this->return['status_code'] = 400;
        $this->return['message'] = [$e->getMessage()];
      }
    }
    return response()->json($this->return);
  }
}
