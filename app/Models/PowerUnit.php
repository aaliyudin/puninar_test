<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PowerUnit extends Model
{
  protected $table = 'power_unit';
  protected $guarded = ['id_power_unit'];

  protected  $primaryKey = 'id_power_unit';

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function Location()
  {
    return $this->belongsTo('App\Models\Location','id_location','id_location');
  }

  public function Corporation()
  {
    return $this->belongsTo('App\Models\Corporation','id_corporation','id_corporation');
  }

  public function PowerUnitType()
  {
    return $this->belongsTo('App\Models\PowerUnitType','id_power_unit_type','id_power_unit_type');
  }

}
