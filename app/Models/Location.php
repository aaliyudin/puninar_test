<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
  protected $table = 'location';
  protected $guarded = ['id_location'];

  protected  $primaryKey = 'id_location';

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function PowerUnit()
  {
      return $this->hasMany(PowerUnit::class);
  }
}
