<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PowerUnitType extends Model
{
  protected $table = 'power_unit_type';
  protected $guarded = ['id_power_unit_type'];

  protected  $primaryKey = 'id_power_unit_type';

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function PowerUnit()
  {
      return $this->hasMany(PowerUnit::class);
  }
}
