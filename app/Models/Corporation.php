<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Corporation extends Model
{
  protected $table = 'corporation';
  protected $guarded = ['id_corporation'];

  protected  $primaryKey = 'id_corporation';

  public function user()
  {
    return $this->belongsTo(User::class);
  }

  public function PowerUnit()
  {
      return $this->hasMany(PowerUnit::class);
  }
}
