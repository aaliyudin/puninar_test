@extends('layouts.app')

@section('title', 'Add New Power Unit')

@section('content')

<h1>Add Power Unit</h1>

<form action="{{ route('power_unit.store') }}" method="post" enctype="multipart/form-data">
    @include('power_units._partials.form')
</form>

@endsection
