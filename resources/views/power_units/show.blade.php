@extends('layouts.app')

@section('title', ": {$power_unit->power_unit_num}")

@section('content')

<h1>Delete Power Unit <b>{{ $power_unit->power_unit_num }}</b></h1>

<p>{{ $power_unit->description }}</p>

<form action="{{ route('power_unit.destroy', $power_unit->id_power_unit) }}" method="post">
    @csrf
    <input type="hidden" name="_method" value="DELETE">
    <button type="submit" class="btn btn-danger">Delete Power Unit: {{ $power_unit->power_unit_num }}</button>
</form>

@endsection
