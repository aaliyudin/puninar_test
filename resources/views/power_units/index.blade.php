@extends('layouts.app')

@section('title', 'Listagem dos posts')

@section('content')

<h1>
  Power Units
  <a href="{{ route('power_unit.create') }}" class="btn btn-primary">
    <i class="fas fa-plus-square"></i>
  </a>
</h1>

@include('includes.alerts')

<ul class="media-list">
  @php
  $i = 1;
  @endphp
  <table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Power Unit Num</th>
        <th scope="col">Description</th>
        <th scope="col">Power Unit Type</th>
        <th scope="col">Corporation</th>
        <th scope="col">Location</th>
      </tr>
    </thead>
    <tbody>
      @forelse($power_units as $power_unit)
      <tr>
        <th scope="row">{{$i}}</th>
        <td>{{$power_unit->power_unit_num}}</td>
        <td>{{$power_unit->description}}</td>
        <td>{{$power_unit->PowerUnitType->power_unit_type_xid}}</td>
        <td>{{$power_unit->Corporation->corporation_name}}</td>
        <td>{{$power_unit->Location->location_name}}</td>
        <td>
          <a href="{{ route('power_unit.destroy', $power_unit->id_power_unit) }}">Delete</a> |
          <a href="{{ route('power_unit.edit', $power_unit->id_power_unit) }}">Edit</a>
        </td>
      </tr>
      <hr>
      @php
      $i++;
      @endphp
      @empty
      <li class="media">
        <p>No Data!</p>
      </li>
      @endforelse
    </tbody>
  </table>
  {!! $power_units->links() !!}
</ul>

@endsection
