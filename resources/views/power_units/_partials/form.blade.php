@include('includes.alerts')

@csrf
<div class="form-group">
    <input value="{{ $data['power_unit']->power_unit_num ?? old('power_unit_num') }}" class="form-control" type="text" name="power_unit_num" placeholder="No">
</div>
<div class="form-group">
    <input value="{{ $data['power_unit']->description ?? old('description') }}" class="form-control" type="text" name="description" placeholder="Description">
</div>
<div class="form-group">
    <select class="form-control" name="id_power_unit_type">
      @foreach($data['power_unit_types'] as $power_unit_type)
      <option value="{{$power_unit_type->id_power_unit_type}}" {{(isset($data['power_unit']) && $power_unit_type->id_power_unit_type == $data['power_unit']->id_power_unit_type) ? 'selected' : ''}}>{{$power_unit_type->power_unit_type_xid}}</option>
      @endforeach
    </select>
</div>
<div class="form-group">
    <select class="form-control" name="id_corporation">
      @foreach($data['corporations'] as $corporation)
      <option value="{{$corporation->id_corporation}}" {{(isset($data['power_unit']) && $corporation->id_corporation == $data['power_unit']->id_corporation) ? 'selected' : ''}}>{{$corporation->corporation_name}}</option>
      @endforeach
    </select>
</div>
<div class="form-group">
    <select class="form-control" name="id_location">
      @foreach($data['locations'] as $location)
      <option value="{{$location->id_location}}" {{(isset($data['power_unit']) && $location->id_location == $data['power_unit']->id_location) ? 'selected' : ''}}>{{$location->location_name}}</option>
      @endforeach
    </select>
</div>
<div class="form-group">
    <button type="submit" class="btn btn-success">Submit</button>
</div>
