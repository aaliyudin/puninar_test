@extends('layouts.app')

@section('title', "Edit Power Unit {$data['power_unit']->power_unit_num}")

@section('content')

<h1>Edit Power Unit: {{ $data['power_unit']->power_unit_num }}</h1>

<form action="{{ route('power_unit.update', $data['power_unit']->id_power_unit) }}" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_method" value="PUT">

    @include('power_units._partials.form')
</form>

@endsection
