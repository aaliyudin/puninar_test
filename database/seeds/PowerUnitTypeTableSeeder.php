<?php

use Illuminate\Database\Seeder;

class PowerUnitTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('power_unit_type')->insert([
        [
          'id_power_unit_type' => 1,
          'power_unit_type_xid' => 'ENGKEL',
          'description' => 'PRIME_MOVER ENGKEL NON KAROSERI',
          'insert_user' => 1,
          'update_user' => 1
        ],
        [
          'id_power_unit_type' => 2,
          'power_unit_type_xid' => 'TRONTON',
          'description' => 'PRIME_MOVER ENGKEL NON KAROSERI',
          'insert_user' => 1,
          'update_user' => 1
        ],
      ]);
    }
}
