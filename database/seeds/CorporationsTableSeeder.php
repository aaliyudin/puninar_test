<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CorporationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('corporation')->insert([
        [
          'id_corporation' => 1,
          'corporation_name' => 'PT. PUNINAR JAYA',
          'insert_user' => 1,
          'update_user' => 1
        ],
        [
          'id_corporation' => 2,
          'corporation_name' => 'PT. PUNINAR INFINITE RAYA',
          'insert_user' => 1,
          'update_user' => 1
        ],
      ]);
    }
}
