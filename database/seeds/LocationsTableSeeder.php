<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('location')->insert([
        [
          'id_location' => 1,
          'location_name' => 'POOL CILEGON',
          'city' => 'CILEGON',
          'province' => 'BANTEN',
          'latitude' => '-6.002534',
          'longitude' => '106.011124',
          'insert_user' => 1,
          'update_user' => 1
        ],
        [
          'id_location' => 2,
          'location_name' => 'SURABAYA CILEGON',
          'city' => 'PANDAAN',
          'province' => 'JAWA TIMUR',
          'latitude' => '-7.6503',
          'longitude' => '112.7057',
          'insert_user' => 1,
          'update_user' => 1
        ],
      ]);
    }
}
