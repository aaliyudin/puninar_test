<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePowerUnitTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('power_unit_type', function (Blueprint $table) {
          $table->increments('id_power_unit_type');
          $table->string('power_unit_type_xid')->unique();
          $table->string('description');
          $table->integer('insert_user');
          $table->integer('update_user');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('power_unit_type');
    }
}
