<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePowerUnitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('power_unit', function (Blueprint $table) {
          $table->increments('id_power_unit');
          $table->string('power_unit_num');
          $table->string('description');
          $table->integer('id_corporation');
          $table->integer('id_location');
          $table->integer('id_power_unit_type');
          $table->integer('insert_user');
          $table->integer('update_user')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('power_unit');
    }
}
