<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix'=>'v1'], function ($router) {
  Route::post('register', 'API\RegisterController@register');
  Route::post('login', 'API\LoginController@login');
});
Route::group(['prefix'=>'v1', 'middleware'=>'jwt.auth'], function ($router) {
  Route::resource('power_unit', 'API\PowerUnitController',['only'=>[
    'store','index','update','destroy'
  ]]);
});
